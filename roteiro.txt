git help: Mostra vários comandos de forma resumida

git help <comando>: Mostra detalhes de um comando específico

git config --global user.name “Seu Nome”
git config --global user.email “seuemaill@gmail.com”   (Usar o mesmo email do gitlab)
 
Para o repositório atual: --local;
Para o Usuário: --global;
Para o Computador: --system

Opcional:
(primeiro testa code -v)
git config --global core.editor 'code --wait' -> Vscode para editor padrão (precisa do code no PATH)


	Se tudo der certo:
	git config --global -e -> Abre o arquivo de configuração no vscode

-------------------------------------------------------


git init: Inicia ou cria um repositorio

git status: Permite visualizar o estado do repositório

git add arquivo/diretório : Prepara o conteúdo descrito no arquivo para o próximo commit
git add --all = git add . : Prepara todos os conteúdos ainda não comitados para o próximo commit

git commit -m “Primeiro commit” : Salva o conteúdo atual junto com uma mensagem de registro do usuário que descreve as alterações.

-------------------------------------------------------

git log :  Exibe um histórico de commits.
git log arquivo: Exibe um historico de commit para um arquivo específico.
git reflog: Exibe uma lista de hashes, que representam onde você esteve durante commits.

-------------------------------------------------------

git show : Exibe um ou mais objetos (bolhas, árvores, tags e commits).
git show <commit> :  Exibe a mensagem do registro log e a diferença textual.

-------------------------------------------------------

git diff : mostra as linhas exatas que foram adicionadas e removidas
git diff <commit1> <commit2>: Mostra a diferença entre dois commits

-------------------------------------------------------

git branch: Listar todas as ramificações no seu repositório
git branch -r : Redefina <branchname> para <start-point>, mesmo que <branchname> exista já
git branch -a : Listar todas as ramificações remotas.
git branch -d <branch_name> : Excluir a ramificação especificada.
git branch -D <branch_name>: Forçar a exclusão da ramificação especificada, mesmo que ela tenha mudanças não mescladas.
git branch -m <nome_novo> Renomear a ramificação atual para <nome_novo>
git branch -m <nome_antigo> <nome_novo>: Renomear um branch local a partir de outro branch 

-------------------------------------------------------

git checkout <branch_name> : Mudar de branch.
git checkout -b <branch_name> Mudar de branch por uma branch que ainda não existe.

-------------------------------------------------------

git merge <branch_name> juntar a branch com a master.

-------------------------------------------------------

git clone: clonando um repositorio existente
git pull: Incorpora as alterações de um repositório remoto no branch atual.
git push: Comando em que você transfere commits a partir do seu repositório local para um repositório remoto.

-------------------------------------------------------

git remote: Permite criar, ver e excluir conexões com outros repositórios.
git remote -v: Igual o git remote, mas inclui a URL de cada conexão.
git remote add origin <url>: Crie uma nova conexão com um repositório remoto.
git remote rename <old-name> <new-name> Renomeie uma conexão remota de ＜old-name＞ para ＜new-name＞.

--------------------------------------------------------

Documentação do git:
https://git-scm.com/doc

Playlist GIT:
https://www.youtube.com/playlist?list=PLxjCnO-f9JPT6Kuww_d2TJQK31Mz-cwR4
